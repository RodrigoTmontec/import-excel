<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 05/11/2018
 * Time: 12:05
 */

namespace App\Dao;
ini_set('max_execution_time', -1);

use App\src\Conexao\Conexao;

class UserDao
{
    private $conexao;

    public function __construct()
    {
        $this->conexao = Conexao::getInstance();
    }

    public function selectUser ()
    {
       $sql = "SELECT * FROM usuarios";
       $prepare = $this->conexao->prepare($sql);
       $prepare->execute();
       $dados = $prepare->fetchAll(\PDO::FETCH_ASSOC);
       return $dados;
    }
}