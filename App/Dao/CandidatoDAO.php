<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 12/09/2018
 * Time: 10:25
 */

namespace App\Dao;

use App\Model\Candidato;
use App\src\Conexao\Conexao;

class CandidatoDao
{
    private $conexao;

    public function __construct()
    {
        $this->conexao = Conexao::getInstance();
    }

    public function inserir(Candidato $candidato)
    {
        $sql = "INSERT INTO candidatos (name, 
               lastname, 
               cpf,
               rg, 
               email, 
               foto, 
               cv_anexo, 
               sexo, 
               telefone, 
               celular, 
               telefone_add, 
               has_cnh, 
               cep, 
               endereco,
               complemento, 
               numero, 
               bairro, 
               cidade, 
               estado, 
               regiao, 
               id_area_profissional, 
               objetivo, 
               ultimo_cargo, 
               ensino_medio, 
               status, 
               possuiVeiculo, 
               password, 
               remember_token, 
               type, 
               data_nasc, 
               step, 
               complete, 
               created_at, 
               updated_at, 
               deleted_at
               ) 
               VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $prepare = $this->conexao->prepare($sql);

        $prepare->bindValue(1, $candidato->getName());
        $prepare->bindValue(2, $candidato->getLastname());
        $prepare->bindValue(3, $candidato->getCpf());
        $prepare->bindValue(4, $candidato->getRg());
        $prepare->bindValue(5, $candidato->getEmail());
        $prepare->bindValue(6, $candidato->getFoto());
        $prepare->bindValue(7, $candidato->getCvAnexo());
        $prepare->bindValue(8, $candidato->getSexo());
        $prepare->bindValue(9, $candidato->getTelefone());
        $prepare->bindValue(10,$candidato->getCelular());
        $prepare->bindValue(11,$candidato->getTelefoneAdd());
        $prepare->bindValue(12,$candidato->getHasCnh());
        $prepare->bindValue(13,$candidato->getCep());
        $prepare->bindValue(14,$candidato->getEndereco());
        $prepare->bindValue(15,$candidato->getComplemento());
        $prepare->bindValue(16,$candidato->getNumero());
        $prepare->bindValue(17,$candidato->getBairro());
        $prepare->bindValue(18,$candidato->getCidade());
        $prepare->bindValue(19,$candidato->getEstado());
        $prepare->bindValue(20,$candidato->getRegiao());
        $prepare->bindValue(21,$candidato->getIdAreaProfissional());
        $prepare->bindValue(22,$candidato->getObjetivo());
        $prepare->bindValue(23,$candidato->getUltimoCargo());
        $prepare->bindValue(24,$candidato->getEnsinoMedio());
        $prepare->bindValue(25,$candidato->getStatus());
        $prepare->bindValue(26,$candidato->getPossuiVeiculo());
        $prepare->bindValue(27,$candidato->getPassword());
        $prepare->bindValue(28,$candidato->getRememberToken());
        $prepare->bindValue(29,$candidato->getType());
        $prepare->bindValue(30,$candidato->getDataNasc());
        $prepare->bindValue(31,$candidato->getStep());
        $prepare->bindValue(32,$candidato->getComplete());
        $prepare->bindValue(33,$candidato->getCreatedAt());
        $prepare->bindValue(34,$candidato->getUpdatedAt());
        $prepare->bindValue(35,$candidato->getDeletedAt());

        if($prepare->execute()) {
            return true;
        } else {
            return false;
        }
    }
}