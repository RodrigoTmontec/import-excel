<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 12/09/2018
 * Time: 10:17
 */

namespace App\Model;


use Carbon\Carbon;

class Candidato
{
    private $id;
    private $name;
    private $lastname;
    private $cpf;
    private $rg;
    private $email;
    private $foto;
    private $cv_anexo;
    private $sexo;
    private $telefone;
    private $celular;
    private $telefone_add;
    private $has_cnh;
    private $cep;
    private $endereco;
    private $complemento;
    private $numero;
    private $bairro;
    private $cidade;
    private $estado;
    private $regiao;
    private $id_area_profissional;
    private $objetivo;
    private $ultimo_cargo;
    private $ensino_medio;
    private $status;
    private $possuiVeiculo;
    private $password;
    private $remember_token;
    private $type;
    private $data_nasc;
    private $step;
    private $complete;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    /**
     * Candidato constructor.
     * @param $name
     * @param $lastname
     * @param $cpf
     * @param $rg
     * @param $email
     * @param $foto
     * @param $cv_anexo
     * @param $sexo
     * @param $telefone
     * @param $celular
     * @param $telefone_add
     * @param $has_cnh
     * @param $cep
     * @param $endereco
     * @param $complemento
     * @param $numero
     * @param $bairro
     * @param $cidade
     * @param $estado
     * @param $regiao
     * @param $id_area_profissional
     * @param $objetivo
     * @param $ultimo_cargo
     * @param $ensino_medio
     * @param $status
     * @param $possuiVeiculo
     * @param $password
     * @param $remember_token
     * @param $type
     * @param $data_nasc
     * @param $step
     * @param $complete
     * @param $created_at
     * @param $updated_at
     * @param $deleted_at
     */
    public function __construct(User $user)
    {
        $nome                       = explode(' ',$user->getNome());
        $this->name                 = $nome[0];
        $this->lastname             = $nome[sizeof($nome) - 1];
        $this->cpf                  = $user->getCpf();
        $this->rg                   = $user->getRg();
        $this->email                = $user->getEmail();
        $this->foto                 = ' ';
        $this->cv_anexo             = ' ';
        $this->sexo                 = $user->getSexo();
        $this->telefone             = $user->getTelefoneResidencial();
        $this->celular              = $user->getTelefoneCelular();
        $this->telefone_add         = $user->getTelefoneAdicional();
        $this->has_cnh              = $user->getCarteiraTipoA();
        $this->cep                  = $user->getCep();
        $this->endereco             = $user->getEndereco();
        $this->complemento          = $user->getComplemento();
        $this->numero               = 'S/N';
        $this->bairro               = $user->getBairro();
        $this->cidade               = $user->getIdCidade();
        $this->estado               = $user->getIdEstado();
        $this->regiao               = $user->getIdZona();
        $this->id_area_profissional = 1;
        $this->objetivo             = $user->getObjetivoProfissional();
        $this->ultimo_cargo         = '';
        $this->ensino_medio         = $user->getEnsinoMedio();
        $this->status               = 1;
        $this->possuiVeiculo        = $user->getPossuiVeiculo();
        $this->password             = $user->getPwdSenha1();
        $this->remember_token       = '';
        $this->type                 = 'Candidato';
        $this->data_nasc            = $user->getDataNascimento();
        $this->step                 = 1;
        $this->complete             = $user->getComplemento();
        $this->created_at           = Carbon::now();
        $this->updated_at           = Carbon::now();
        $this->deleted_at           = '';
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param mixed $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @return mixed
     */
    public function getCvAnexo()
    {
        return $this->cv_anexo;
    }

    /**
     * @param mixed $cv_anexo
     */
    public function setCvAnexo($cv_anexo)
    {
        $this->cv_anexo = $cv_anexo;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getTelefoneAdd()
    {
        return $this->telefone_add;
    }

    /**
     * @param mixed $telefone_add
     */
    public function setTelefoneAdd($telefone_add)
    {
        $this->telefone_add = $telefone_add;
    }

    /**
     * @return mixed
     */
    public function getHasCnh()
    {
        return $this->has_cnh;
    }

    /**
     * @param mixed $has_cnh
     */
    public function setHasCnh($has_cnh)
    {
        $this->has_cnh = $has_cnh;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getRegiao()
    {
        return $this->regiao;
    }

    /**
     * @param mixed $regiao
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;
    }

    /**
     * @return mixed
     */
    public function getIdAreaProfissional()
    {
        return $this->id_area_profissional;
    }

    /**
     * @param mixed $id_area_profissional
     */
    public function setIdAreaProfissional($id_area_profissional)
    {
        $this->id_area_profissional = $id_area_profissional;
    }

    /**
     * @return mixed
     */
    public function getObjetivo()
    {
        return $this->objetivo;
    }

    /**
     * @param mixed $objetivo
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;
    }

    /**
     * @return mixed
     */
    public function getUltimoCargo()
    {
        return $this->ultimo_cargo;
    }

    /**
     * @param mixed $ultimo_cargo
     */
    public function setUltimoCargo($ultimo_cargo)
    {
        $this->ultimo_cargo = $ultimo_cargo;
    }

    /**
     * @return mixed
     */
    public function getEnsinoMedio()
    {
        return $this->ensino_medio;
    }

    /**
     * @param mixed $ensino_medio
     */
    public function setEnsinoMedio($ensino_medio)
    {
        $this->ensino_medio = $ensino_medio;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPossuiVeiculo()
    {
        return $this->possuiVeiculo;
    }

    /**
     * @param mixed $possuiVeiculo
     */
    public function setPossuiVeiculo($possuiVeiculo)
    {
        $this->possuiVeiculo = $possuiVeiculo;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * @param mixed $remember_token
     */
    public function setRememberToken($remember_token)
    {
        $this->remember_token = $remember_token;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDataNasc()
    {
        return $this->data_nasc;
    }

    /**
     * @param mixed $data_nasc
     */
    public function setDataNasc($data_nasc)
    {
        $this->data_nasc = $data_nasc;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     */
    public function setStep($step)
    {
        $this->step = $step;
    }

    /**
     * @return mixed
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @param mixed $complete
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @param mixed $deleted_at
     */
    public function setDeletedAt($deleted_at)
    {
        $this->deleted_at = $deleted_at;
    }
}