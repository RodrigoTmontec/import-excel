<?php

namespace App\Model;

class User
{
   private $id;
   private $email;
   private $pwd_senha1;
   private $pwd_senha2;
   private $nome;
   private $data_nascimento;
   private $endereco;
   private $complemento;
   private $bairro;
   private $cep;
   private $telefone_residencial;
   private $telefone_celular;
   private $telefone_adicional;
   private $cpf;
   private $rg;
   private $carteira_tipo_a;
   private $carteira_tipo_b;
   private $carteira_tipo_c;
   private $carteira_tipo_d;
   private $carteira_tipo_e;
   private $instituicao_tecnico;
   private $instituicao_superior;
   private $objetivo_profissional;
   private $resumo_profissional;
   private $resumo_extra_curricular;
   private $sexo;
   private $id_cidade;
   private $id_estado;
   private $id_zona;
   private $possui_carteira;
   private $possui_veiculo;
   private $ensino_medio;
   private $ensino_tecnico;
   private $ensino_superior;
   private $id_curso_tecnico;
   private $id_curso_superior;
   private $ano_ensino_medio;
   private $ano_previsaotermino_ensino_medio;
   private $mes_previsaotermino_ensino_medio;
   private $ano_ensino_tecnico;
   private $ano_previsaotermino_ensino_tecnico;
   private $mes_previsaotermino_ensino_tecnico;
   private $ano_ensino_superior;
   private $ano_previsaotermino_ensino_superior;
   private $mes_previsaotermino_ensino_superior;
   private $periodo_ensino_medio;
   private $periodo_ensino_tecnico;
   private $periodo_ensino_superior;
   private $empregado;
   private $data_hora_envio;
   private $ip;

    /**
     * User constructor.
     * @param $id
     * @param $email
     * @param $pwd_senha1
     * @param $pwd_senha2
     * @param $nome
     * @param $data_nascimento
     * @param $endereco
     * @param $complemento
     * @param $bairro
     * @param $cep
     * @param $telefone_residencial
     * @param $telefone_celular
     * @param $telefone_adicional
     * @param $cpf
     * @param $rg
     * @param $carteira_tipo_a
     * @param $carteira_tipo_b
     * @param $carteira_tipo_c
     * @param $carteira_tipo_d
     * @param $carteira_tipo_e
     * @param $instituicao_tecnico
     * @param $instituicao_superior
     * @param $objetivo_profissional
     * @param $resumo_profissional
     * @param $resumo_extra_curricular
     * @param $sexo
     * @param $id_cidade
     * @param $id_estado
     * @param $id_zona
     * @param $possui_carteira
     * @param $possui_veiculo
     * @param $ensino_medio
     * @param $ensino_tecnico
     * @param $ensino_superior
     * @param $id_curso_tecnico
     * @param $id_curso_superior
     * @param $ano_ensino_medio
     * @param $ano_previsaotermino_ensino_medio
     * @param $mes_previsaotermino_ensino_medio
     * @param $ano_ensino_tecnico
     * @param $ano_previsaotermino_ensino_tecnico
     * @param $mes_previsaotermino_ensino_tecnico
     * @param $ano_ensino_superior
     * @param $ano_previsaotermino_ensino_superior
     * @param $mes_previsaotermino_ensino_superior
     * @param $periodo_ensino_medio
     * @param $periodo_ensino_tecnico
     * @param $periodo_ensino_superior
     * @param $empregado
     * @param $data_hora_envio
     * @param $ip
     */
    public function __construct($usuario)
    {
        $this->email                               = $usuario['email'];
        $this->pwd_senha1                          = '123456';
        $this->pwd_senha2                          = $usuario['pwd_senha2'];
        $this->nome                                = $usuario['nome'];
        $this->data_nascimento                     = $usuario['data_nascimento'];
        $this->endereco                            = $usuario['endereco'];
        $this->complemento                         = $usuario['complemento'];
        $this->bairro                              = $usuario['bairro'];
        $this->cep                                 = $usuario['cep'];
        $this->telefone_residencial                = $usuario['telefone_residencial'];
        $this->telefone_celular                    = $usuario['telefone_celular'];
        $this->telefone_adicional                  = $usuario['telefone_adicional'];
        $this->cpf                                 = $usuario['cpf'];
        $this->rg                                  = $usuario['rg'];
        $this->carteira_tipo_a                     = $usuario['carteira_tipo_a'];
        $this->carteira_tipo_b                     = $usuario['carteira_tipo_b'];
        $this->carteira_tipo_c                     = $usuario['carteira_tipo_c'];
        $this->carteira_tipo_d                     = $usuario['carteira_tipo_d'];
        $this->carteira_tipo_e                     = $usuario['carteira_tipo_e'];
        $this->instituicao_tecnico                 = $usuario['instituicao_tecnico'];
        $this->instituicao_superior                = $usuario['instituicao_superior'];
        $this->objetivo_profissional               = $usuario['objetivo_profissional'];
        $this->resumo_profissional                 = $usuario['resumo_profissional'];
        $this->resumo_extra_curricular             = $usuario['resumo_extra_curricular'];
        $this->sexo                                = $usuario['sexo'];
        $this->id_cidade                           = $usuario['id_cidade'];
        $this->id_estado                           = $usuario['id_estado'];
        $this->id_zona                             = $usuario['id_zona'];
        $this->possui_carteira                     = $usuario['possui_carteira'];
        $this->possui_veiculo                      = $usuario['possui_veiculo'];
        $this->ensino_medio                        = $usuario['ensino_medio'];
        $this->ensino_tecnico                      = $usuario['ensino_tecnico'];
        $this->ensino_superior                     = $usuario['ensino_superior'];
        $this->id_curso_tecnico                    = $usuario['id_curso_tecnico'];
        $this->id_curso_superior                   = $usuario['id_curso_superior'];
        $this->ano_ensino_medio                    = $usuario['ano_ensino_medio'];
        $this->ano_previsaotermino_ensino_medio    = $usuario['ano_previsaotermino_ensino_medio'];
        $this->mes_previsaotermino_ensino_medio    = $usuario['mes_previsaotermino_ensino_medio'];
        $this->ano_ensino_tecnico                  = $usuario['ano_ensino_tecnico'];
        $this->ano_previsaotermino_ensino_tecnico  = $usuario['ano_previsaotermino_ensino_tecnico'];
        $this->mes_previsaotermino_ensino_tecnico  = $usuario['mes_previsaotermino_ensino_tecnico'];
        $this->ano_ensino_superior                 = $usuario['ano_ensino_superior'];
        $this->ano_previsaotermino_ensino_superior = $usuario['ano_previsaotermino_ensino_superior'];
        $this->mes_previsaotermino_ensino_superior = $usuario['mes_previsaotermino_ensino_superior'];
        $this->periodo_ensino_medio                = $usuario['periodo_ensino_medio'];
        $this->periodo_ensino_tecnico              = $usuario['periodo_ensino_tecnico'];
        $this->periodo_ensino_superior             = $usuario['periodo_ensino_superior'];
        $this->empregado                           = $usuario['empregado'];
        $this->data_hora_envio                     = $usuario['data_hora_envio'];
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPwdSenha1()
    {
        return $this->pwd_senha1;
    }

    /**
     * @param mixed $pwd_senha1
     */
    public function setPwdSenha1($pwd_senha1)
    {
        $this->pwd_senha1 = $pwd_senha1;
    }

    /**
     * @return mixed
     */
    public function getPwdSenha2()
    {
        return $this->pwd_senha2;
    }

    /**
     * @param mixed $pwd_senha2
     */
    public function setPwdSenha2($pwd_senha2)
    {
        $this->pwd_senha2 = $pwd_senha2;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDataNascimento()
    {
        return $this->data_nascimento;
    }

    /**
     * @param mixed $data_nascimento
     */
    public function setDataNascimento($data_nascimento)
    {
        $this->data_nascimento = $data_nascimento;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getTelefoneResidencial()
    {
        return $this->telefone_residencial;
    }

    /**
     * @param mixed $telefone_residencial
     */
    public function setTelefoneResidencial($telefone_residencial)
    {
        $this->telefone_residencial = $telefone_residencial;
    }

    /**
     * @return mixed
     */
    public function getTelefoneCelular()
    {
        return $this->telefone_celular;
    }

    /**
     * @param mixed $telefone_celular
     */
    public function setTelefoneCelular($telefone_celular)
    {
        $this->telefone_celular = $telefone_celular;
    }

    /**
     * @return mixed
     */
    public function getTelefoneAdicional()
    {
        return $this->telefone_adicional;
    }

    /**
     * @param mixed $telefone_adicional
     */
    public function setTelefoneAdicional($telefone_adicional)
    {
        $this->telefone_adicional = $telefone_adicional;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param mixed $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return mixed
     */
    public function getCarteiraTipoA()
    {
        return $this->carteira_tipo_a;
    }

    /**
     * @param mixed $carteira_tipo_a
     */
    public function setCarteiraTipoA($carteira_tipo_a)
    {
        $this->carteira_tipo_a = $carteira_tipo_a;
    }

    /**
     * @return mixed
     */
    public function getCarteiraTipoB()
    {
        return $this->carteira_tipo_b;
    }

    /**
     * @param mixed $carteira_tipo_b
     */
    public function setCarteiraTipoB($carteira_tipo_b)
    {
        $this->carteira_tipo_b = $carteira_tipo_b;
    }

    /**
     * @return mixed
     */
    public function getCarteiraTipoC()
    {
        return $this->carteira_tipo_c;
    }

    /**
     * @param mixed $carteira_tipo_c
     */
    public function setCarteiraTipoC($carteira_tipo_c)
    {
        $this->carteira_tipo_c = $carteira_tipo_c;
    }

    /**
     * @return mixed
     */
    public function getCarteiraTipoD()
    {
        return $this->carteira_tipo_d;
    }

    /**
     * @param mixed $carteira_tipo_d
     */
    public function setCarteiraTipoD($carteira_tipo_d)
    {
        $this->carteira_tipo_d = $carteira_tipo_d;
    }

    /**
     * @return mixed
     */
    public function getCarteiraTipoE()
    {
        return $this->carteira_tipo_e;
    }

    /**
     * @param mixed $carteira_tipo_e
     */
    public function setCarteiraTipoE($carteira_tipo_e)
    {
        $this->carteira_tipo_e = $carteira_tipo_e;
    }

    /**
     * @return mixed
     */
    public function getInstituicaoTecnico()
    {
        return $this->instituicao_tecnico;
    }

    /**
     * @param mixed $instituicao_tecnico
     */
    public function setInstituicaoTecnico($instituicao_tecnico)
    {
        $this->instituicao_tecnico = $instituicao_tecnico;
    }

    /**
     * @return mixed
     */
    public function getInstituicaoSuperior()
    {
        return $this->instituicao_superior;
    }

    /**
     * @param mixed $instituicao_superior
     */
    public function setInstituicaoSuperior($instituicao_superior)
    {
        $this->instituicao_superior = $instituicao_superior;
    }

    /**
     * @return mixed
     */
    public function getObjetivoProfissional()
    {
        return $this->objetivo_profissional;
    }

    /**
     * @param mixed $objetivo_profissional
     */
    public function setObjetivoProfissional($objetivo_profissional)
    {
        $this->objetivo_profissional = $objetivo_profissional;
    }

    /**
     * @return mixed
     */
    public function getResumoProfissional()
    {
        return $this->resumo_profissional;
    }

    /**
     * @param mixed $resumo_profissional
     */
    public function setResumoProfissional($resumo_profissional)
    {
        $this->resumo_profissional = $resumo_profissional;
    }

    /**
     * @return mixed
     */
    public function getResumoExtraCurricular()
    {
        return $this->resumo_extra_curricular;
    }

    /**
     * @param mixed $resumo_extra_curricular
     */
    public function setResumoExtraCurricular($resumo_extra_curricular)
    {
        $this->resumo_extra_curricular = $resumo_extra_curricular;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getIdCidade()
    {
        return $this->id_cidade;
    }

    /**
     * @param mixed $id_cidade
     */
    public function setIdCidade($id_cidade)
    {
        $this->id_cidade = $id_cidade;
    }

    /**
     * @return mixed
     */
    public function getIdEstado()
    {
        return $this->id_estado;
    }

    /**
     * @param mixed $id_estado
     */
    public function setIdEstado($id_estado)
    {
        $this->id_estado = $id_estado;
    }

    /**
     * @return mixed
     */
    public function getIdZona()
    {
        return $this->id_zona;
    }

    /**
     * @param mixed $id_zona
     */
    public function setIdZona($id_zona)
    {
        $this->id_zona = $id_zona;
    }

    /**
     * @return mixed
     */
    public function getPossuiCarteira()
    {
        return $this->possui_carteira;
    }

    /**
     * @param mixed $possui_carteira
     */
    public function setPossuiCarteira($possui_carteira)
    {
        $this->possui_carteira = $possui_carteira;
    }

    /**
     * @return mixed
     */
    public function getPossuiVeiculo()
    {
        return $this->possui_veiculo;
    }

    /**
     * @param mixed $possui_veiculo
     */
    public function setPossuiVeiculo($possui_veiculo)
    {
        $this->possui_veiculo = $possui_veiculo;
    }

    /**
     * @return mixed
     */
    public function getEnsinoMedio()
    {
        return $this->ensino_medio;
    }

    /**
     * @param mixed $ensino_medio
     */
    public function setEnsinoMedio($ensino_medio)
    {
        $this->ensino_medio = $ensino_medio;
    }

    /**
     * @return mixed
     */
    public function getEnsinoTecnico()
    {
        return $this->ensino_tecnico;
    }

    /**
     * @param mixed $ensino_tecnico
     */
    public function setEnsinoTecnico($ensino_tecnico)
    {
        $this->ensino_tecnico = $ensino_tecnico;
    }

    /**
     * @return mixed
     */
    public function getEnsinoSuperior()
    {
        return $this->ensino_superior;
    }

    /**
     * @param mixed $ensino_superior
     */
    public function setEnsinoSuperior($ensino_superior)
    {
        $this->ensino_superior = $ensino_superior;
    }

    /**
     * @return mixed
     */
    public function getIdCursoTecnico()
    {
        return $this->id_curso_tecnico;
    }

    /**
     * @param mixed $id_curso_tecnico
     */
    public function setIdCursoTecnico($id_curso_tecnico)
    {
        $this->id_curso_tecnico = $id_curso_tecnico;
    }

    /**
     * @return mixed
     */
    public function getIdCursoSuperior()
    {
        return $this->id_curso_superior;
    }

    /**
     * @param mixed $id_curso_superior
     */
    public function setIdCursoSuperior($id_curso_superior)
    {
        $this->id_curso_superior = $id_curso_superior;
    }

    /**
     * @return mixed
     */
    public function getAnoEnsinoMedio()
    {
        return $this->ano_ensino_medio;
    }

    /**
     * @param mixed $ano_ensino_medio
     */
    public function setAnoEnsinoMedio($ano_ensino_medio)
    {
        $this->ano_ensino_medio = $ano_ensino_medio;
    }

    /**
     * @return mixed
     */
    public function getAnoPrevisaoterminoEnsinoMedio()
    {
        return $this->ano_previsaotermino_ensino_medio;
    }

    /**
     * @param mixed $ano_previsaotermino_ensino_medio
     */
    public function setAnoPrevisaoterminoEnsinoMedio($ano_previsaotermino_ensino_medio)
    {
        $this->ano_previsaotermino_ensino_medio = $ano_previsaotermino_ensino_medio;
    }

    /**
     * @return mixed
     */
    public function getMesPrevisaoterminoEnsinoMedio()
    {
        return $this->mes_previsaotermino_ensino_medio;
    }

    /**
     * @param mixed $mes_previsaotermino_ensino_medio
     */
    public function setMesPrevisaoterminoEnsinoMedio($mes_previsaotermino_ensino_medio)
    {
        $this->mes_previsaotermino_ensino_medio = $mes_previsaotermino_ensino_medio;
    }

    /**
     * @return mixed
     */
    public function getAnoEnsinoTecnico()
    {
        return $this->ano_ensino_tecnico;
    }

    /**
     * @param mixed $ano_ensino_tecnico
     */
    public function setAnoEnsinoTecnico($ano_ensino_tecnico)
    {
        $this->ano_ensino_tecnico = $ano_ensino_tecnico;
    }

    /**
     * @return mixed
     */
    public function getAnoPrevisaoterminoEnsinoTecnico()
    {
        return $this->ano_previsaotermino_ensino_tecnico;
    }

    /**
     * @param mixed $ano_previsaotermino_ensino_tecnico
     */
    public function setAnoPrevisaoterminoEnsinoTecnico($ano_previsaotermino_ensino_tecnico)
    {
        $this->ano_previsaotermino_ensino_tecnico = $ano_previsaotermino_ensino_tecnico;
    }

    /**
     * @return mixed
     */
    public function getMesPrevisaoterminoEnsinoTecnico()
    {
        return $this->mes_previsaotermino_ensino_tecnico;
    }

    /**
     * @param mixed $mes_previsaotermino_ensino_tecnico
     */
    public function setMesPrevisaoterminoEnsinoTecnico($mes_previsaotermino_ensino_tecnico)
    {
        $this->mes_previsaotermino_ensino_tecnico = $mes_previsaotermino_ensino_tecnico;
    }

    /**
     * @return mixed
     */
    public function getAnoEnsinoSuperior()
    {
        return $this->ano_ensino_superior;
    }

    /**
     * @param mixed $ano_ensino_superior
     */
    public function setAnoEnsinoSuperior($ano_ensino_superior)
    {
        $this->ano_ensino_superior = $ano_ensino_superior;
    }

    /**
     * @return mixed
     */
    public function getAnoPrevisaoterminoEnsinoSuperior()
    {
        return $this->ano_previsaotermino_ensino_superior;
    }

    /**
     * @param mixed $ano_previsaotermino_ensino_superior
     */
    public function setAnoPrevisaoterminoEnsinoSuperior($ano_previsaotermino_ensino_superior)
    {
        $this->ano_previsaotermino_ensino_superior = $ano_previsaotermino_ensino_superior;
    }

    /**
     * @return mixed
     */
    public function getMesPrevisaoterminoEnsinoSuperior()
    {
        return $this->mes_previsaotermino_ensino_superior;
    }

    /**
     * @param mixed $mes_previsaotermino_ensino_superior
     */
    public function setMesPrevisaoterminoEnsinoSuperior($mes_previsaotermino_ensino_superior)
    {
        $this->mes_previsaotermino_ensino_superior = $mes_previsaotermino_ensino_superior;
    }

    /**
     * @return mixed
     */
    public function getPeriodoEnsinoMedio()
    {
        return $this->periodo_ensino_medio;
    }

    /**
     * @param mixed $periodo_ensino_medio
     */
    public function setPeriodoEnsinoMedio($periodo_ensino_medio)
    {
        $this->periodo_ensino_medio = $periodo_ensino_medio;
    }

    /**
     * @return mixed
     */
    public function getPeriodoEnsinoTecnico()
    {
        return $this->periodo_ensino_tecnico;
    }

    /**
     * @param mixed $periodo_ensino_tecnico
     */
    public function setPeriodoEnsinoTecnico($periodo_ensino_tecnico)
    {
        $this->periodo_ensino_tecnico = $periodo_ensino_tecnico;
    }

    /**
     * @return mixed
     */
    public function getPeriodoEnsinoSuperior()
    {
        return $this->periodo_ensino_superior;
    }

    /**
     * @param mixed $periodo_ensino_superior
     */
    public function setPeriodoEnsinoSuperior($periodo_ensino_superior)
    {
        $this->periodo_ensino_superior = $periodo_ensino_superior;
    }

    /**
     * @return mixed
     */
    public function getEmpregado()
    {
        return $this->empregado;
    }

    /**
     * @param mixed $empregado
     */
    public function setEmpregado($empregado)
    {
        $this->empregado = $empregado;
    }

    /**
     * @return mixed
     */
    public function getDataHoraEnvio()
    {
        return $this->data_hora_envio;
    }

    /**
     * @param mixed $data_hora_envio
     */
    public function setDataHoraEnvio($data_hora_envio)
    {
        $this->data_hora_envio = $data_hora_envio;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }
}